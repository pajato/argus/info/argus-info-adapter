package com.pajato.argus.info.adapter

import com.pajato.argus.info.core.I18nStrings.INFO_URI_ERROR_KEY
import com.pajato.argus.info.core.InfoId
import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.TimestampedMovie
import com.pajato.argus.info.core.TimestampedPerson
import com.pajato.argus.info.core.TimestampedTv
import com.pajato.i18n.strings.Arg
import com.pajato.i18n.strings.StringsResource.get
import java.io.File
import java.net.URI
import kotlin.io.path.fileSize
import kotlin.io.path.toPath
import kotlin.test.fail

object TestAgent {
    private const val FAILURE_MESSAGE = "Weird, unexpected software error!"

    fun copyDirs(loader: ClassLoader) {
        val (srcPath, dstPath) = Pair("read-only-files", "files")
        val (srcUrl, dstUrl) = Pair(getUrl(loader, srcPath), getUrl(loader, dstPath))
        val (srcDir, dstDir) = Pair(File(srcUrl.toURI()), File(dstUrl.toURI()))
        srcDir.copyRecursively(dstDir, true)
    }

    fun getJson(loader: ClassLoader, id: InfoId): String {
        val path = "info-$id.json"
        val url = loader.getResource(path) ?: fail("Path $path is not valid!")
        return url.toURI().toPath().toFile().readText()
    }

    fun getUri(loader: ClassLoader, path: String): URI =
        loader.getResource(path)?.toURI() ?: fail(getUriErrorMessage(path))

    fun getFileSize(uri: URI): Long = uri.toPath().fileSize()

    fun getRefreshTime(key: InfoKey) = when (val item = ArgusInfoRepo.cache[key]) {
        is TimestampedMovie -> item.refreshTime
        is TimestampedPerson -> item.refreshTime
        is TimestampedTv -> item.refreshTime
        null -> fail(FAILURE_MESSAGE)
    }

    fun getUriErrorMessage(path: String) = get(INFO_URI_ERROR_KEY, Arg("path", path))

    private fun getUrl(loader: ClassLoader, path: String) = loader.getResource(path) ?: fail(getUriErrorMessage(path))
}
