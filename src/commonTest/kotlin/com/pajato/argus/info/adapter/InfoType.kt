package com.pajato.argus.info.adapter

enum class InfoType { MOVIE, PERSON, TV }
