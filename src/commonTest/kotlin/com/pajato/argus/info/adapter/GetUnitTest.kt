package com.pajato.argus.info.adapter

import com.pajato.argus.info.adapter.TestAgent.copyDirs
import com.pajato.argus.info.adapter.TestAgent.getUri
import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertNotNull
import kotlin.test.assertNull

class GetUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() { copyDirs(loader) }

    @Test fun `When accessing a single, missing item in a tv repo, verify the result`() {
        val key = InfoKey(InfoType.Tv.name, 0)
        runBlocking { ArgusInfoRepo.injectDependency(getUri(loader, "info-1911.json")) }
        assertNull(ArgusInfoRepo.cache[key])
    }

    @Test fun `When accessing a single, present item in a tv repo, verify the result`() {
        val key = InfoKey(InfoType.Tv.name, 1911)
        runBlocking { ArgusInfoRepo.injectDependency(getUri(loader, "info-1911.json")) }
        assertNotNull(ArgusInfoRepo.cache[key])
    }
}
