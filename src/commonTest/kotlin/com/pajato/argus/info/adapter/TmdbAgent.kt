package com.pajato.argus.info.adapter

import kotlinx.coroutines.runBlocking
import java.net.URI
import kotlin.test.fail

private lateinit var content: String
private lateinit var errorMessage: String
private lateinit var errorExc: String
private lateinit var errorExcMessage: String

internal fun fetch(uri: String): String {
    val parts: List<String> = URI(uri).path.substring(1).split("/")
    fetchContent(uri, getKey(parts, uri))
    return content
}

internal fun handleError(message: String, exc: Exception?) {
    errorMessage = message
    errorExc = if (exc == null) "null" else exc.javaClass.name
    errorExcMessage = if (exc == null) "null" else exc.message ?: "null"
}

private fun fetchContent(uri: String, type: InfoType) {
    runBlocking {
        content = URI(uri).toURL().readText()
        if (content.isEmpty()) fail("Could not get content!") else registerItem(type, content)
    }
}
