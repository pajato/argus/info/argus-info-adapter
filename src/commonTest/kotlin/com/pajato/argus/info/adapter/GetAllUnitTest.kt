package com.pajato.argus.info.adapter

import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class GetAllUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() {
        TestAgent.copyDirs(loader)
    }

    @Test fun `When accessing all the items in an info repo, verify the expected size`() {
        val uri = TestAgent.getUri(loader, "files/info.txt")
        runBlocking { ArgusInfoRepo.injectDependency(uri) }
        assertEquals(69, ArgusInfoRepo.cache.values.size)
    }
}
