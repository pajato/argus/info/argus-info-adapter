package com.pajato.argus.info.adapter

import com.pajato.argus.info.adapter.TestAgent.copyDirs
import com.pajato.argus.info.adapter.TestAgent.getJson
import com.pajato.argus.info.adapter.TestAgent.getUri
import com.pajato.argus.info.adapter.TestAgent.getUriErrorMessage
import com.pajato.argus.info.core.InfoRepoError
import com.pajato.argus.info.core.TimestampedTv
import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.tv.core.Tv
import kotlinx.coroutines.runBlocking
import kotlin.io.path.toPath
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class RegisterUnitTest : ReportingTestProfiler() {
    private val loader = this::class.java.classLoader

    @BeforeTest fun setUp() { copyDirs(loader) }

    @Test fun `When no inject has happened, verify the register json exception`() {
        ArgusInfoRepo.dependencyUri = null
        runBlocking {
            assertFailsWith<InfoRepoError> { ArgusInfoRepo.register("") }
                .also { assertEquals(getUriErrorMessage(""), it.message) }
        }
    }

    @Test fun `When no inject has happened, verify the register item exception`() {
        ArgusInfoRepo.dependencyUri = null
        runBlocking {
            assertFailsWith<InfoRepoError> { ArgusInfoRepo.register(TimestampedTv(0L, Tv())) }
                .also { assertEquals(getUriErrorMessage(""), it.message) }
        }
    }

    @Test fun `When registering a tv item using json, verify the info repo file sizes`() {
        val infoUri = getUri(loader, "files/info.txt")
        runBlocking { assertTv(infoUri, getJson(loader, 1911), 245517L) }
    }

    @Test fun `When registering a tv item, verify the info repo file sizes`() {
        val infoUri = getUri(loader, "files/info.txt")
        runBlocking { assertTv(infoUri, 1911, 245516L, loader) }
    }

    @Test fun `When registering a movie item using json, verify the info repo file sizes`() {
        val infoUri = getUri(loader, "files/info.txt")
        runBlocking { assertMovie(infoUri, getJson(loader, 29339), 242778L) }
    }

    @Test fun `When registering a person item using json, verify the info repo file sizes`() {
        val infoUri = getUri(loader, "files/info.txt").also { it.toPath().toFile().writeText("") }
        runBlocking { assertPerson(infoUri, getJson(loader, 190405), 524L) }
    }

    @Test fun `When registering a default tv show, verify an exception`() {
        val infoUri = getUri(loader, "files/info.txt")
        runBlocking {
            ArgusInfoRepo.injectDependency(infoUri)
            assertFailsWith<IllegalArgumentException> { ArgusInfoRepo.register(TimestampedTv(0L, Tv())) }
        }
    }

    @Test fun `When registering with empty json, verify an exception`() {
        val infoUri = getUri(loader, "files/info.txt")
        runBlocking {
            ArgusInfoRepo.injectDependency(infoUri)
            assertFailsWith<IllegalArgumentException> { ArgusInfoRepo.register("") }
        }
    }
}
