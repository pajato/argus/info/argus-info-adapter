package com.pajato.argus.info.adapter

import com.pajato.argus.info.adapter.TestAgent.copyDirs
import com.pajato.argus.info.adapter.TestAgent.getUri
import com.pajato.argus.info.core.I18nStrings
import com.pajato.argus.info.core.I18nStrings.INFO_NOT_A_FILE
import com.pajato.argus.info.core.InfoRepoError
import com.pajato.i18n.strings.Arg
import com.pajato.i18n.strings.StringsResource.get
import com.pajato.test.ReportingTestProfiler
import kotlinx.coroutines.runBlocking
import java.io.File
import kotlin.io.path.toPath
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue
import kotlin.test.fail

class InjectUnitTest : ReportingTestProfiler() {
    private val loader: ClassLoader = this::class.java.classLoader
    private val infoPath = "files/info.txt"

    init { I18nStrings.registerStrings() }

    @BeforeTest fun setUp() { copyDirs(loader) }

    @Test fun `When a URI is not a file, verify an InfoRepoError is thrown`() {
        val uri = getUri(loader, "files")
        val expected = get(INFO_NOT_A_FILE, Arg("path", "${uri.toPath()}"))
        assertFailsWith<InfoRepoError> { runBlocking { ArgusInfoRepo.injectDependency(uri) } }
            .also { assertEquals(expected, it.message) }
    }

    @Test fun `When a URI file does not exist, verify that it is created`() {
        val infoUri = getUri(loader, infoPath)
        val file = infoUri.toPath().toFile().also { it.delete() }
        if (file.exists()) fail("The info file: ${file.absolutePath} could not be deleted!")
        runBlocking { ArgusInfoRepo.injectDependency(infoUri) }
        assertTrue(file.exists())
    }

    @Test fun `When performing a normal inject, verify behavior`() {
        val uri = getUri(loader, infoPath)
        assertEquals(71, File(uri).readLines().size)
        runBlocking { ArgusInfoRepo.injectDependency(uri) }
        assertEquals(69, File(uri).readLines().size)
    }
}
