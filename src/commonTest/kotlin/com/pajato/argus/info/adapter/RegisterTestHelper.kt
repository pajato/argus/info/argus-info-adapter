package com.pajato.argus.info.adapter

import com.pajato.argus.info.core.InfoId
import com.pajato.argus.info.core.InfoWrapper
import com.pajato.argus.info.core.TimestampedTv
import com.pajato.persister.jsonFormat
import java.net.URI
import kotlin.test.assertEquals

internal suspend fun assertTv(infoUri: URI, json: String, size: Long) {
    ArgusInfoRepo.injectDependency(infoUri)
    ArgusInfoRepo.register(json)
    assertEquals(size, TestAgent.getFileSize(infoUri))
}

internal suspend fun assertTv(infoUri: URI, id: InfoId, size: Long, loader: ClassLoader) {
    val tv = jsonFormat.decodeFromString(InfoWrapper.serializer(), TestAgent.getJson(loader, id))
    ArgusInfoRepo.injectDependency(infoUri)
    ArgusInfoRepo.register(tv as TimestampedTv)
    assertEquals(size, TestAgent.getFileSize(infoUri))
}

internal suspend fun assertMovie(infoUri: URI, json: String, size: Long) {
    ArgusInfoRepo.injectDependency(infoUri)
    ArgusInfoRepo.register(json)
    assertEquals(size, TestAgent.getFileSize(infoUri))
}

internal suspend fun assertPerson(infoUri: URI, json: String, size: Long) {
    ArgusInfoRepo.injectDependency(infoUri)
    ArgusInfoRepo.register(json)
    assertEquals(size, TestAgent.getFileSize(infoUri))
}
