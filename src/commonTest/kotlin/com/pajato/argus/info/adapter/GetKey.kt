package com.pajato.argus.info.adapter

import com.pajato.dependency.uri.validator.InjectError

internal fun getKey(parts: List<String>, uri: String): InfoType = when (parts[1]) {
    "tv" -> InfoType.TV
    "movie" -> InfoType.MOVIE
    "person" -> InfoType.PERSON
    else -> throw InjectError("Invalid URI (type): $uri")
}
