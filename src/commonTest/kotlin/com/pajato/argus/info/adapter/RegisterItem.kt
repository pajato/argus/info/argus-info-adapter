package com.pajato.argus.info.adapter

internal suspend fun registerItem(type: InfoType, json: String) = when (type) {
    InfoType.MOVIE -> ArgusInfoRepo.register(json)
    InfoType.PERSON -> ArgusInfoRepo.register(json)
    InfoType.TV -> ArgusInfoRepo.register(json)
}
