package com.pajato.argus.info.adapter

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoType.Movie
import com.pajato.argus.info.core.InfoType.Person
import com.pajato.argus.info.core.InfoType.Tv
import com.pajato.argus.info.core.InfoWrapper
import com.pajato.argus.info.core.TimestampedMovie
import com.pajato.argus.info.core.TimestampedPerson
import com.pajato.argus.info.core.TimestampedTv
import java.io.File
import java.net.URI
import kotlin.collections.set

internal fun register(cache: InfoCache, uri: URI, item: InfoWrapper, json: String) {
    val (file, key) = Pair(File(uri), getInfoKey(item))
    validate(json, key.id)
    cache[key] = item
    file.appendText("$json\n")
}

private fun getInfoKey(item: InfoWrapper) = when (item) {
    is TimestampedMovie -> InfoKey(Movie.name, item.movie.id)
    is TimestampedPerson -> InfoKey(Person.name, item.person.id)
    is TimestampedTv -> InfoKey(Tv.name, item.tv.id)
}

private fun validate(json: String, id: Int) {
    if (id <= 0) throw IllegalArgumentException("Invalid id (must be positive!); JSON: $json, id: $id")
}
