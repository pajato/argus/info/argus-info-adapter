package com.pajato.argus.info.adapter

import com.pajato.argus.info.core.I18nStrings.INFO_NOT_A_FILE
import com.pajato.argus.info.core.I18nStrings.INFO_URI_ERROR_KEY
import com.pajato.argus.info.core.I18nStrings.registerStrings
import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoRepo
import com.pajato.argus.info.core.InfoRepoError
import com.pajato.argus.info.core.InfoType
import com.pajato.argus.info.core.InfoWrapper
import com.pajato.argus.info.core.TimestampedMovie
import com.pajato.argus.info.core.TimestampedPerson
import com.pajato.argus.info.core.TimestampedTv
import com.pajato.dependency.uri.validator.validateUri
import com.pajato.i18n.strings.Arg
import com.pajato.i18n.strings.StringsResource.get
import com.pajato.persister.jsonFormat
import com.pajato.persister.readAndPruneData
import java.net.URI
import kotlin.io.path.toPath

public object ArgusInfoRepo : InfoRepo {
    override val cache: MutableMap<InfoKey, InfoWrapper> = mutableMapOf()
    internal var dependencyUri: URI? = null

    init { registerStrings() }

    override suspend fun injectDependency(uri: URI) {
        validateUri(uri) { throwInfoRepoError(INFO_NOT_A_FILE, Arg("path", "${uri.toPath()}")) }
        readAndPruneData(uri, cache, InfoWrapper.serializer(), ArgusInfoRepo::getKeyFromItem)
        dependencyUri = uri
    }

    override suspend fun register(json: String) {
        val uri = checkUri()
        val item = jsonFormat.decodeFromString(InfoWrapper.serializer(), json)
        register(cache, uri, item, json)
    }

    override suspend fun register(item: InfoWrapper) {
        val uri = checkUri()
        val json = jsonFormat.encodeToString(InfoWrapper.serializer(), item)
        register(cache, uri, item, json)
    }

    private fun getKeyFromItem(item: InfoWrapper) = when (item) {
        is TimestampedMovie -> InfoKey(InfoType.Movie.name, item.movie.id)
        is TimestampedPerson -> InfoKey(InfoType.Person.name, item.person.id)
        is TimestampedTv -> InfoKey(InfoType.Tv.name, item.tv.id)
    }

    private fun checkUri(): URI = dependencyUri ?: throwInfoRepoError(INFO_URI_ERROR_KEY)

    private fun throwInfoRepoError(messageKey: String, arg: Arg? = null): Nothing {
        val message = if (arg != null) get(messageKey, arg) else get(messageKey)
        throw InfoRepoError(message)
    }
}
