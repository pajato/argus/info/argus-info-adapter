package com.pajato.argus.info.adapter

import com.pajato.argus.info.core.InfoKey
import com.pajato.argus.info.core.InfoWrapper

internal typealias InfoCache = MutableMap<InfoKey, InfoWrapper>
