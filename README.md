# argus-info-adapter

## Description

This project implements the [Clean Code
Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) "Interface Adapters"
layer for the info feature. The project is responsible for implementing the interfaces and top
level artifacts supporting the info feature.

The sole responsibility for the info feature is to support the Argus application's ability to present
collections of videos matching a theme, presented in a "shelf". These themes include such things as continue watching,
watchlist, history, paused, finished and hidden. The info feature provides text, icons, images for tv shows, movies and people.

## License

GPL, Version 3.0.  See the peer document LICENSE for details.

## Contributions

See the [contributing guide](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/CONTRIBUTING.md) in the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

## Project status

Converted to Kotlin Multiplatform (KMP) with versions 0.10.*

## Documentation

For general documentation on Argus, see the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

As documentation entered into code files grows stale seconds after it is written, no such documentation is created.
Instead, documentation is created by you on demand using the Dokka Gradle task: 'dokkaGfm'. After successful task
completion, see the detailed documentation [here](build/dokka/gfm/index.md)

## Usage

To use the project, follow these steps:

1. Add the project as a dependency in your build file.
2. Import the necessary classes and interfaces from the project.
3. Use the provided APIs to interact with the shelf feature.

## Test Cases

### Overview

The table below identifies the adapter layer unit tests. A test file name is always of the form `NamePrefixUnitTest.kt`.
The test file content is one or more test cases (functions). All tests are in the `com.pajato.argus.info.adapter` package.

| Filename Prefix | Test Case Name                                                                              |
|-----------------|---------------------------------------------------------------------------------------------|
| GetAll          | When accessing all the items in an info repo, verify the expected size                      |
| Get             | When accessing a single, missing item in a tv repo, verify the result                       |
|                 | When accessing a single, present item in a tv repo, verify the result                       |
| Inject          | When a URI is not a file, verify an InfoRepoError is thrown                                 |
|                 | When a URI file does not exist, verify that it is created                                   |
|                 | When performing a normal inject, verify behavior                                            |
| Refresh         | When no inject has happened, verify the refresh check exception                             |
|                 | When cache access fails, verify the refresh check exception                                 |
|                 | When a timestamped movie instance is stale, verify two successive updates behave correctly  |
|                 | When a timestamped person instance is stale, verify two successive updates behave correctly |
|                 | When a timestamped tv instance is stale, verify two successive updates behave correctly     |
| Register        | When no inject has happened, verify the register json exception                             |
|                 | When registering a tv item using json, verify the info repo file sizes                      |
|                 | When registering a tv item, verify the info repo file sizes                                 |
|                 | When registering a movie item using json, verify the info repo file sizes                   |
|                 | When registering a person item using json, verify the info repo file sizes                  |



### Notes

Examples of implemented video info interfaces are ArgusInfoKey, ArgusTvKey, ArgusMovieKey, ArgusSeriesKey,
ArgusEpisodeKey and ArgusInfoRepo.
